import edu.uoc.taep.BoardWithExceptions;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class KenKenBoardWithExceptionsTest {

    @Test
    public void testNegativeRow(){
        BoardWithExceptions board = new BoardWithExceptions();
        try{
            board.set(-1,1,3);
        } catch(IllegalArgumentException e){
            assertEquals("Illegal row -1", e.getMessage());
        }
    }

    @Test
    public void testRowGreaterThan6(){
        BoardWithExceptions board = new BoardWithExceptions();
        try{
            board.set(7,1,3);
        } catch(IllegalArgumentException e){
            assertEquals("Illegal row 7", e.getMessage());
        }
    }

    @Test
    public void testNegativeColumn(){
        BoardWithExceptions board = new BoardWithExceptions();
        try{
            board.set(1,-1,3);
        } catch(IllegalArgumentException e){
            assertEquals("Illegal column -1", e.getMessage());
        }
    }

    @Test
    public void testColumnGreaterThan6(){
        BoardWithExceptions board = new BoardWithExceptions();
        try{
            board.set(1,7,3);
        } catch(IllegalArgumentException e){
            assertEquals("Illegal column 7", e.getMessage());
        }
    }

    @Test
    public void testNegativeValue(){
        BoardWithExceptions board = new BoardWithExceptions();
        try{
            board.set(1,1,-1);
        } catch(IllegalArgumentException e){
            assertEquals("Illegal value -1", e.getMessage());
        }
    }

    @Test
    public void testValueGreaterThan6(){
        BoardWithExceptions board = new BoardWithExceptions();
        try{
            board.set(1,1,7);
        } catch(IllegalArgumentException e){
            assertEquals("Illegal value 7", e.getMessage());
        }
    }

    @Test
    public void testRepeatedValue(){
        BoardWithExceptions board = new BoardWithExceptions();
        board.set(1,2,6);
        try{
            board.set(1,3,6);
        } catch(IllegalArgumentException e){
            assertEquals("Illegal value 6. It is already present at row 1 column 2.", e.getMessage());
        }
    }

}
