import edu.uoc.taep.Area;
import edu.uoc.taep.Board;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class KenKenBoardTest {

    @Test
    public void testNegativeRow(){
        Board board = createTestBoard();
        assertEquals(-1, board.set(-1, 1, 3));
    }

    @Test
    public void testRowGreaterThan6(){
        Board board = createTestBoard();
        assertEquals(-1, board.set(7,1,3));
    }

    @Test
    public void testNegativeColumn(){
        Board board = createTestBoard();
        assertEquals(-1, board.set(1,-1,3));
    }

    @Test
    public void testColumnGreaterThan6(){
        Board board = createTestBoard();
        assertEquals(-1, board.set(1,7,3));
    }

    @Test
    public void testNegativeValue(){
        Board board = createTestBoard();
        assertEquals(-1, board.set(1,1,-1));
    }

    @Test
    public void testValueGreaterThan6(){
        Board board = createTestBoard();
        assertEquals(-1, board.set(1,1,7));
    }

    @Test
    public void testRepeatedValue(){
        Board board = createTestBoard();
        board.set(1,2,6);
        assertEquals(-1, board.set(1, 3, 6));
        assertEquals(0, board.get(1,3));
    }
    
    @Test
    public void testCorrectMove(){
        Board board = createTestBoard();
        assertEquals(0, board.set(1, 2, 5));
        assertEquals(5, board.get(1,2));
    }

    @Test
    public void testFinalMove(){
        Board board = createTestBoard();
        fillAllBoardExcept33(board);
        System.out.println(board);
        assertEquals(2,board.set(3,3,5));
        assertEquals(5, board.get(3,3));
    }

    /** Creates a test board with 6 areas, one per row
     *  All the areas will have operation + and result 21 */
    private Board createTestBoard(){
        Board board = new Board();
        for(int row = 1; row <= 6; row ++){
            Area a = board.createArea("+",21);
            for(int col = 1; col <=6; col++){
                a.addSquare(row,col);
            }
        }
        return board;
    }

    /** Fills all the board except for cell 3,3
     *  Assigns valid rows to every cell except for the empty cell.
     *  The empty cell expects a value of 5.
     *  @param board The board to be filled
     */
    private void fillAllBoardExcept33(Board board) {
        for(int row = 1; row <= 6; row++){
            for(int col = 1; col <= 6; col++){
                if(row != 3 || col != 3){
                    board.set(row, col, 1 + (col+row-2) % 6);
                }
            }
        }
    }
}
