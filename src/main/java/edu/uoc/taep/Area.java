package edu.uoc.taep;

import java.util.HashSet;
import java.util.Set;

class Square{

    private final int row;
    private final int col;

    public Square(int row, int col) {
        this.row = row;
        this.col = col;
    }
}

public class Area {

    private final String operation;
    private final int result;
    private final Set<Square> squares = new HashSet<Square>();

    public Area(String operation, int result) {
        this.operation = operation;
        this.result = result;
    }

    public void addSquare(int row, int col) {
        squares.add(new Square(row,col));
    }
}
