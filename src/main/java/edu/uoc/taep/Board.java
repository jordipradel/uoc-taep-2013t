package edu.uoc.taep;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Board {

    private Map<Integer,Map<Integer,Integer>> cells = new HashMap<Integer,Map<Integer,Integer>>();
    private final Set<Area> areas = new HashSet<Area>();

    public Board(){
        for(int row = 1; row < 7; row++){
            cells.put(row, new HashMap<Integer, Integer>());
        }
    }

    public int set(int row, int column, int value) {
        if(row < 0 || row > 6) return -1;
        if(column < 0 || column > 6) return -1;
        if(value < 0 || value > 6) return -1;
        if(isRepeated(row,column,value)) return -1;
        cells.get(row).put(column,value);
        if(isComplete()) return 2;
        return 0;
    }

    private boolean isComplete(){
        for(int row = 1; row < 7; row ++){
            for(int col = 1; col < 7; col ++){
                if(get(row,col) == 0) return false;
            }
        }
        return true;
    }

    private boolean isRepeated(int row, int col, int value){
        for(int otherRow = 1; otherRow < 7; otherRow++){
            if(get(otherRow,col) == value) return true;
        }
        for(int otherColumn = 1; otherColumn < 7; otherColumn++){
            if(get(row,otherColumn) == value) return true;
        }
        return false;
    }

    public Area createArea(String operation, int result) {
        Area a = new Area(operation,result);
        areas.add(a);
        return a;
    }

    public int get(int row, int column) {
        Integer result = cells.get(row).get(column);
        return result == null ? 0 : result;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        for(int row = 1; row < 7; row ++){
            for(int col = 1; col < 7; col ++){
                sb.append(get(row,col));
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
